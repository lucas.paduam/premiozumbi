# PremioZumbi_DWEL5_IFSP

Projeto desenvolvido para disciplina de Desenvolvimento Web I, 5º semestre, curso Análise e Desenvolvimento de Sistemas, Instituto Federeal de São Paulo.


## Tecnologias Utilizadas

- [VSCode](https://code.visualstudio.com/)
- [HTML5](https://html.spec.whatwg.org/multipage/)
- [CSS3](https://www.w3.org/Style/CSS/Overview.en.html)
- [JavaScript](https://www.javascript.com/)
- [jQuery](https://jquery.com/)

